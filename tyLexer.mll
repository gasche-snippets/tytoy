{
  open TyParser

  let keywords = [
    "type", TYPE;
    "and", AND;
  ]

  let newline lexbuf =
    let open Lexing in
    lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with
                             pos_lnum = lexbuf.lex_curr_p.pos_lnum + 1;
                             pos_bol =  lexbuf.lex_curr_p.pos_cnum;
                         }
}


let newline = ('\n' | '\r' | "\r\n" "\n\r")
let any_ident_char = ['0'-'9'  'a'-'z'  'A'-'Z'  '_'  '\'']

rule token = parse
| [' ' '\t']+
    { token lexbuf }
| newline
    { newline lexbuf;
      token lexbuf }
| ['a'-'z'] any_ident_char* as s
    { try List.assoc s keywords
      with Not_found -> IDENT s }
| '='
    { EQUAL }
| '*'
    { STAR }
| '+'
    { PLUS }
| "->"
    { ARROW }
| '('
    { LPAREN }
| ')'
    { RPAREN }
| eof
    { EOF }
