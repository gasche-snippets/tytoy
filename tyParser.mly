%{
  open TyAst
%}

%token TYPE AND
%token ARROW STAR PLUS EQUAL
%token <string>IDENT
%token LPAREN RPAREN
%token EOF

%start <TyAst.defs list> defs

%%

defs:
| li=list(mutual_def) EOF { li }

mutual_def:
| head=def(TYPE) rest=list(def(AND)) { head::rest }
def(kwd):
| kwd constr=IDENT params=list(IDENT) EQUAL body=ty
  { (constr, params, body)}

ty:
| ty=func { ty }
func:
| a=sum ARROW b=func { Fun(a,b) }
| ty=sum { ty }
sum:
| a=sum PLUS b=prod { Sum(a,b) }
| ty=prod { ty }
prod:
| a=prod STAR b=app { Prod(a,b) }
| ty=app { ty }
app:
| constr=IDENT args=nonempty_list(atom) { App(constr, args) }
| ty=atom { ty }
atom:
| id=IDENT { Var id }
| LPAREN RPAREN { Unit }
| LPAREN t=ty RPAREN { t }
