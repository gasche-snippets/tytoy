let input_from_file_option file =
  let name, chan =
    match file with
      | Some filename ->
        filename, open_in filename
      | None -> "", stdin in
  let lexbuf = Lexing.from_channel chan in
  lexbuf.Lexing.lex_curr_p <- {
    Lexing.pos_fname = name;
    Lexing.pos_lnum  = 1;
    Lexing.pos_bol   = 0;
    Lexing.pos_cnum  = 0
  };
  lexbuf

let process input =
  let program =
    try TyParser.defs TyLexer.token input
    with TyParser.Error ->
      let open Lexing in
      let pos = input.lex_curr_p in
      Printf.eprintf "Syntax error %s, line %d, column %d.\n"
        (match pos.pos_fname with "" -> "on stdin"
          | name -> Printf.sprintf "in file %S" name)
        pos.pos_lnum (pos.pos_cnum - pos.pos_bol);
      exit 1
  in
  program |> List.iter (fun defs ->
    TyPrinter.(doc_defs defs |> print)
  )

let () =
  let inputs =
    match List.tl @@ Array.to_list Sys.argv with
      | [] -> [input_from_file_option None]
      | tl ->
        tl |> List.map  (fun file ->
          input_from_file_option (Some file)
        )
  in
  List.iter process inputs
