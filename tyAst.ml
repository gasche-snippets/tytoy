type typ =
  | Sum of typ * typ                  (* ty1 + ty2 *)
  | Prod of typ * typ                 (* ty1 * ty2 *)
  | Fun of typ * typ                  (* ty1 -> ty2 *)
  | Unit                              (* unit *)
  | Var of string                     (* a *)
  | App of string * typ list          (* foo ty1 ty2 ty3 *)

type defs =
  (string * string list * typ) list   (* type foo a b c = ...
                                         and bar a b = ...     *)

let def_list =
  [ "list", ["a"], Sum (Unit, Prod (Var "a", App ("list", [Var "a"]))) ]

let def_other =
  [ "t", ["a"; "b"], Sum (Fun (Var "a", Var "b"), App ("u", [Prod (Var "a", Var "b"); Var "b"]));
    "u", ["c"; "d"], App ("t", [Var "c"; Var "c"]);
  ]
