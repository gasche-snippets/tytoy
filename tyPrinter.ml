open TyAst
open PPrint

let doc_op symb ty1 ty2 = infix 2 1 symb ty1 ty2
let doc_app f xs = prefix 2 1 f (separate (break 1) xs)

let doc_typ ty =
  let rec typ ty = func ty
  and func = function
    | Fun(a,b) -> doc_op (string "->") (sum a) (func b)
    | ty -> sum ty
  and sum = function
    | Sum (a, b) -> doc_op plus (sum a) (prod b)
    | ty -> prod ty
  and prod = function
    | Prod (a, b) -> doc_op star (prod a) (app b)
    | ty -> (app ty)
  and app = function
    | App(constr, args) -> doc_app (string constr) (List.map atom args)
    | ty -> atom ty
  and atom = function
    | Var id -> string id
    | Unit -> string "()"
    | ty -> group (parens (typ ty))
  in typ ty

let doc_def kwd (name, params, body) =
  let params = List.map string params in
  prefix 2 1
    (separate space [string kwd; doc_app (string name) params; equals])
    (doc_typ body)

let doc_defs = function
  | [] -> empty
  | def::defs ->
    separate (break 1)
      (doc_def "type" def :: List.map (doc_def "and") defs)

let print ?(len=60) doc =
  ToChannel.pretty 1. len stdout doc
;;
